define monit::plugin (
    String $group = $title,
    String $template = 'monit/plugin.epp',
    Array[Hash] $plugin_arr,
) {
    $conf_dir = lookup('monit::conf_dir')

    file {"${conf_dir}/${group}":
        ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp($template, {
	    'plugins' => $plugin_arr,
	}),
	notify => Service['monit'],
	require => Package['monit'],
    }
}
