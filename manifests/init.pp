class monit (
    Boolean $enable,
    String $ensure,
    Integer $interval,
    Integer $httpd_port,
    String $mailserver,
    String $id_file,
    String $conf_dir,
    String $conf_file,
    Array[String] $httpd_allow,
) {
    package {'monit': ensure => installed, }

    service {'monit':
        ensure => $ensure,
        enable => $enable,
	require => Package['monit'],
    }

    file {"${conf_file}":
        ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0600',
	content => epp('monit/monitrc.epp', {
	    'id_file' => $id_file,
	    'conf_dir' => $conf_dir,
	    'interval' => $interval,
	    'httpd_port' => $httpd_port,
	    'mailserver' => $mailserver,
	    'httpd_allow' => $httpd_allow,
	}),
	notify => Service['monit'],
	require => Package['monit'],
    }
}
